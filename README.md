## Web application

### Goal
Create rest application that shows airport's shedule flights.

### Tools
* [Apache Maven](https://maven.apache.org/) is a software project management and comprehension tool.
* [Eclipse Jetty](https://www.eclipse.org/jetty) provides a Web server and javax.servlet container, plus support for HTTP/2, WebSocket, OSGi, JMX, JNDI, JAAS and many other integrations.
* [JUnit ](http://junit.org/junit4) is a simple framework to write repeatable tests. It is an instance of the xUnit architecture for unit testing frameworks. 

### Installiation and Execution
To start rest application open your *Terminal* and type `bash start.sh` in it.

``$ cd {working directory}``   
``$ bash start.sh``

Open your browser and type `localhost:8080` in address field, if you see " Welcome to British airport " that means your application is working.

### Rules for contributors
The big request to participants not to write in the file README.md any crap and dregs, add only useful text.

## REST framework

Framework that implements [REST](https://en.wikipedia.org/wiki/Representational_state_transfer) standard in java.
Standart called [JAX-RS](https://jcp.org/en/jsr/detail?id=339) and reference implementation called [Jersey](https://jersey.github.io/).
