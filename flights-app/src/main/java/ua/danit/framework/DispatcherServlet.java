package ua.danit.framework;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;

import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import ua.danit.framework.invocations.Invocation;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;


public class DispatcherServlet extends HttpServlet {

  Map<String, Invocation> resources = new HashMap<>();
  Map<String, File> files = Maps.newHashMap();

  /**
   * DispatcherServlet.
   * This designer DispatchServlet constructor
   */

  public DispatcherServlet(Class<?>... classes) {
    for (Class<?> someClass : classes) {
      resources.putAll(Parser.getClassInvocations(someClass));
    }
  }

  public void addResource(Class<?> rClass) {
    this.resources.putAll(Parser.getClassInvocations(rClass));
  }


  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    String loginName = null;

    if (req.getCookies() != null) {

      for (Cookie cookie : req.getCookies()) {
        String cookieName = cookie.getName();
        if (cookieName != null && cookieName.equals("flight-user")) {
          loginName = cookie.getValue();
        }
      }
    }

    if (loginName == null) {
      resp.sendRedirect("/login");
    } else {
      String uri = req.getRequestURI();

      Invocation invoc = resources.get(uri);

      if (resources.containsKey(uri) && canHandleGetMethod(invoc)) {
        handleMethod(req, resp, invoc);
      } else if (files.containsKey(uri)) {
        File file = files.get(uri);
        resp.getWriter().write(Files.toString(file, Charsets.UTF_8));
      } else {
        resp.sendError(404);
      }
    }
  }


  private boolean canHandleGetMethod(Invocation invoc) {
    return invoc != null && invoc.getHttpMethod() instanceof GET;
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String uri = req.getRequestURI();

    Invocation invoc = resources.get(uri);

    if (canHandlePostMethod(invoc)) {
      handleMethod(req, resp, invoc);
    } else {
      resp.sendError(404);
    }
  }

  private boolean canHandlePostMethod(Invocation invoc) {
    return invoc.getHttpMethod() instanceof POST;
  }

  private void handleMethod(HttpServletRequest req, HttpServletResponse resp, Invocation invoc)
      throws IOException {

    String result = hasQueryParams(req) ? getHttpResult(req, invoc) : invoc.invoke();
    resp.getWriter().write(result);
  }

  private String getHttpResult(HttpServletRequest req, Invocation invoc) {
    String result = null;

    for (QueryParam queryParam : invoc.getQueryParamList()) {
      String name = req.getParameter(queryParam.value());
      result = invoc.invoke(name);
    }

    return result;
  }

  private boolean hasQueryParams(HttpServletRequest req) {
    return req.getQueryString() != null;
  }

  public DispatcherServlet addFileMapping(String url, File file) {
    checkNotNull(url, "URL is Null.");
    checkNotNull(file, "FILE is Null.");
    String message = String.format("File [%s] doest not exist.", file.getAbsolutePath());
    checkState(file.exists(), message);
    files.put(url, file);
    return this;
  }


}