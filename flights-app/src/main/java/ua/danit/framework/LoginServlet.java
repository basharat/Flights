package ua.danit.framework;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

  private static final String loginForm =
      "<html><body>" +
      " <form method = 'post'>" +
      " <input name = 'login' type = 'text'><br>" +
      " <input name = 'password' type = 'password'><br>" +
      " <button type = 'submit'>Submit</button><br>" +
      "</form></body></html>";

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String loginName = null;

    if (req.getCookies() != null) {

      for (Cookie cookie : req.getCookies()) {
        String cookieName = cookie.getName();
        if (cookieName != null && cookieName.equals("flight-user")) {
          loginName = cookie.getValue();
        }
      }
    }

    resp.getWriter().write(loginName == null
        || loginName.isEmpty() ? loginForm : loginName);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String login = req.getParameter("login");
    String password = req.getParameter("password");
    resp.addCookie(new Cookie("flight-user", login));
  }
}
