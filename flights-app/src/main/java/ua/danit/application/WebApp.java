package ua.danit.application;

import java.io.File;
import java.io.IOException;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ua.danit.application.dao.FlightDao;
import ua.danit.application.resources.FlightResource;
import ua.danit.framework.DispatcherServlet;
import ua.danit.framework.LoginServlet;

public class WebApp {

    /**
     * This method is the entry point of our application ...
     * creates a server, creates a handler, parser classes
     */

    public static void main(String[] args) throws Exception {

        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();

        DispatcherServlet servlet = new DispatcherServlet(Resource.class)
                .addFileMapping("/", new File("flights-app/resources/index.html"))
                .addFileMapping("/bootstrap.css", new File("flights-app/resources/bootstrap.min.css"))
                .addFileMapping("/jquery.min.js", new File("flights-app/resources/js/jquery.min.js"));

        servlet.addResource(FlightResource.class);

        handler.addServlet(new ServletHolder(servlet), "/*");

        LoginServlet loginServlet = new LoginServlet();
        handler.addServlet(new ServletHolder(loginServlet), "/login");

        server.setHandler(handler);
        server.start();
        server.join();
    }

    public static void main1(String[] args) throws IOException {
//    String flights = new FlightResource(new FlightDao()).getTopFlights();
//    System.out.println(flights);
    }
}
