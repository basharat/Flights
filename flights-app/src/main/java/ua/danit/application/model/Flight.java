package ua.danit.application.model;

public class Flight {
  private Long id;
  private Long departureTime;
  private String from;
  private String to;
  private Long arrivalTime;

  public Flight(Long id, String from, String to, Long departureTime, Long arrivalTime) {
    this.id = id;
    this.from = from;
    this.to = to;
    this.departureTime = departureTime;

    this.arrivalTime = arrivalTime;
  }

  public Long getId() {
    return id;
  }

  public Long getDepartureTime() {
    return departureTime;
  }

  public String getFrom() {
    return from;
  }

  public String getTo() {
    return to;
  }

  public Long getArrivalTime() {
    return arrivalTime;
  }
}
