package ua.danit.application.dao;

import com.google.common.collect.Lists;
import ua.danit.application.model.Flight;

import java.sql.*;
import java.util.List;

public class FlightDao {

  public Iterable<Flight> getTopFlight(int limit) {
    List<Flight> result = Lists.newArrayList();


    String PASSWD = "danit1";
    String DB_URL = "jdbc:mysql://mysql8.db4free.net:3307/danit_flights?useSSL=false";
    String USER = "danit1";
    try {
      Connection con = DriverManager.getConnection(DB_URL,USER,PASSWD);
      String sql = "select id, `from`, `to`, `DEPARTURE_TIME`, `ARRIVAL_TIME` from FLIGHTS";
      Statement statement = con.createStatement();
      ResultSet resultSet = statement.executeQuery(sql);

      while (resultSet.next()) {

        Long id = resultSet.getLong("id");
        Long departureTime = resultSet.getLong("DEPARTURE_TIME");
        Long arrivalTime = resultSet.getLong("ARRIVAL_TIME");
        String from = resultSet.getString("from");
        String to = resultSet.getString("to");

        Flight flight = new Flight(id, from, to, departureTime, arrivalTime);
        result.add(flight);
      }

      resultSet.close();
      statement.close();
      con.close();


    } catch (SQLException e) {
      throw new RuntimeException(e);
    }


    return result;
  }
}
